﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPress : MonoBehaviour {

	public GameObject FailSafe;
	public GameObject Btn;
	Button btn1;
	public GameObject Target;
	bool btn1Triggered = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Target.activeSelf == true && FailSafe.activeSelf == false) {
			btn1 = Btn.GetComponentInChildren<Button> ();
			btn1.onClick.Invoke ();
			FailSafe.SetActive (true);
			Screen.orientation = ScreenOrientation.Portrait;
		}
	}
		
		



}
