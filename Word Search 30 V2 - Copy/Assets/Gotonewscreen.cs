﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gotonewscreen : MonoBehaviour {

	public GameObject failSafe;
	public GameObject scene;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void newScreen(){
		failSafe.SetActive (true);
		scene.SetActive (true);
	}
}
