﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace WordSearch
{
	[CustomEditor(typeof(OptionButton))]
	public class OptionButtonEditor : UnityEditor.UI.ButtonEditor
	{
		public override void OnInspectorGUI ()
		{
			base.OnInspectorGUI();

			serializedObject.Update();

			EditorGUILayout.LabelField("Text Settings", EditorStyles.boldLabel);

			EditorGUILayout.PropertyField(serializedObject.FindProperty("uiText"));
			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(serializedObject.FindProperty("textNormalColor"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("textHighlightedColor"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("textPressedColor"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("textDisabledColor"));
			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(serializedObject.FindProperty("bkgSelectedColor"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("bkgNormalColor"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("textSelectedColor"));
			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(serializedObject.FindProperty("canvasGroup"));

			serializedObject.ApplyModifiedProperties();
		}
	}
}
