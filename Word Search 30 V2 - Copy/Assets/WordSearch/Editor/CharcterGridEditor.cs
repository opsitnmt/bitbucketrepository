﻿using UnityEditor;

namespace WordSearch
{
	[CustomEditor(typeof(CharacterGrid))]
	public class CharcterGridEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			EditorGUILayout.Space();

			EditorGUILayout.LabelField("Gird Layout Settings:");
			EditorGUI.indentLevel++;
			EditorGUILayout.PropertyField(serializedObject.FindProperty("maxCellSize"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("spacing"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("padding"));
			EditorGUI.indentLevel--;

			EditorGUILayout.Space();

			EditorGUILayout.LabelField("Letter Settings:");
			EditorGUI.indentLevel++;
			EditorGUILayout.PropertyField(serializedObject.FindProperty("letterFont"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("letterFontSize"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("letterColor"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("letterOffsetInCell"));
			EditorGUI.indentLevel--;

			EditorGUILayout.Space();

			EditorGUILayout.LabelField("Highlight Settings:");
			EditorGUI.indentLevel++;
			EditorGUILayout.PropertyField(serializedObject.FindProperty("highlightSprite"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("highlightExtraSize"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("highlightAlpha"));
			EditorGUILayout.PropertyField(serializedObject.FindProperty("highlightType"));
			DrawHighlighTypeSettings();
			EditorGUI.indentLevel--;

			EditorGUILayout.Space();

			serializedObject.ApplyModifiedProperties();
		}

		private void DrawHighlighTypeSettings()
		{
			int type = serializedObject.FindProperty("highlightType").enumValueIndex;

			switch (type)
			{
			case 1:
				SerializedProperty redMin	= serializedObject.FindProperty("redMin");
				SerializedProperty redMax	= serializedObject.FindProperty("redMax");
				SerializedProperty greenMin	= serializedObject.FindProperty("greenMin");
				SerializedProperty greenMax	= serializedObject.FindProperty("greenMax");
				SerializedProperty blueMin	= serializedObject.FindProperty("blueMin");
				SerializedProperty blueMax	= serializedObject.FindProperty("blueMax");

				int redMinPrev		= redMin.intValue;
				int greenMinPrev	= greenMin.intValue;
				int blueMinPrev		= blueMin.intValue;

				EditorGUILayout.PropertyField(redMin);
				EditorGUILayout.PropertyField(redMax);
				EditorGUILayout.PropertyField(greenMin);
				EditorGUILayout.PropertyField(greenMax);
				EditorGUILayout.PropertyField(blueMin);
				EditorGUILayout.PropertyField(blueMax);

				SetRange(redMin, redMax, redMinPrev);
				SetRange(greenMin, greenMax, greenMinPrev);
				SetRange(blueMin, blueMax, blueMinPrev);

				break;
			case 2:
				EditorGUILayout.PropertyField(serializedObject.FindProperty("customColors"), true);
				break;
			}
		}

		private void SetRange(SerializedProperty min, SerializedProperty max, int minPrev)
		{
			if (min.intValue > max.intValue)
			{
				if (min.intValue != minPrev)
				{
					// min moved above max, set max
					max.intValue = min.intValue;
				}
				else
				{
					// max moved below min, set min
					min.intValue = max.intValue;
				}
			}
		}
	}
}
