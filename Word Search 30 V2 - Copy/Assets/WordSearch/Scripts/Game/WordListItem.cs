﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace WordSearch
{
	public class WordListItem : MonoBehaviour
	{
		#region Inspector Variables

		public Text			wordText;
		public GameObject	foundIndicator;

		#endregion
	}
}
