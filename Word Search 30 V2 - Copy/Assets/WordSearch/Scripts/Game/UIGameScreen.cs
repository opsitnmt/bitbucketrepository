﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

namespace WordSearch
{
	public class UIGameScreen : UIScreen
	{
		#region Inspector Variables
		public GameObject FailSafe;
		[SerializeField] private GameObject loadingObject;
		[SerializeField] private Popup		completePopup;
		[SerializeField] private Text		completeObjectText;

        public TextMeshProUGUI GameTitle;
        public Text SubTitle;
        public Image BackgroundImage;
       // public Image BannerImage;
		#endregion

		#region Unity Methods

		public override void Initialize()
		{
			WordSearchController.Instance.OnBoardStateChanged += UpdateUI;
            SetUpGameTitle();

        }

		#endregion

		#region Public Methods

		public override void OnShowing (string data)
		{
			//UpdateUI();
		}

		#endregion

		#region Public Methods

		public void OnNewGameButtonClicked()
		{
			WordSearchController.Instance.StartCategory(WordSearchController.Instance.ActiveCategory);
		}

		public void OnMainMenuButtonClicked()
		{
			UIScreenController.Instance.Show(UIScreenController.MainScreenId, true);
            SetUpGameTitle();
			FailSafe.SetActive (false);
			Screen.orientation = ScreenOrientation.LandscapeRight;
		}

        public void SetUpGameTitle()
        {
          //  BannerImage.enabled = true;
            //Because we want a colorful text we need to give the color value as it fallows for each latter <#8DC73DFF>
            GameTitle.text = "<#4536FBFF>W<#24F662FF>o<#F6DD55FF>r<#DA2828FF>d <#AD218EFF>S<#00AEEDFF>e<#8DC73DFF>a<#F6A017FF>r<#EF4136FF>c<#AD218EFF>h";
            SubTitle.text = "Main Menu";
        }

        public void SetUpGameTitle(string CategoryName)
        {
           // BannerImage.enabled = false;
            GameTitle.text = CategoryName;
           // SubTitle.text = "";
        }

        #endregion

        #region Private Methods
        public float invokeTime;
        private void UpdateUI()
		{
			loadingObject.SetActive(WordSearchController.Instance.ActiveBoardState == WordSearchController.BoardState.Generating);

			bool isCompleted	= WordSearchController.Instance.ActiveBoardState == WordSearchController.BoardState.Completed;
			//bool isGameOver		= WordSearchController.Instance.ActiveBoardState == WordSearchController.BoardState.TimesUp;

			if (isCompleted)
			{
                Invoke("PopupShow", 2f);

			}
			else
			{
				completePopup.Hide();
			}

			completeObjectText.text = isCompleted ? "Complete!" : "Times Up!";
		}

        void PopupShow()
        {
            StartCoroutine(PopUpShowCo());
        }

        public Animator celebAnimation;
        public AudioClip[] celebSound;
        public AudioSource auSoruce;

        IEnumerator PopUpShowCo()
        {
            yield return new WaitForSeconds(OnWordFound.instance.duration);
            MusicManager.instance.audioSource.volume = 0;
            celebAnimation.SetTrigger("Celeb");
            auSoruce.clip = celebSound[Random.Range(0, celebSound.Length)];
            auSoruce.Play();
            yield return new WaitForSeconds(auSoruce.clip.length);
            MusicManager.instance.audioSource.volume = 0.15f;
            
            //completePopup.Show();
        }
		#endregion
	}
}
