﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoScene : MonoBehaviour {

    public Animation logoAnimation;
    float timer;
	
	// Update is called once per frame
	void Update () {

        timer += Time.deltaTime;
        if(Input.GetButtonDown("Fire1") && timer > logoAnimation.clip.length)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
	}
}
