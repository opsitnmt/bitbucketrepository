﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace WordSearch
{
	public class CharacterGridItem : MonoBehaviour
	{
		#region Inspector Variables

		public Text	characterText;

		#endregion

		#region Member Variables
		#endregion

		#region Properties

		public int Row { get; set; }
		public int Col { get; set; }

		#endregion

		#region Unity Methods
		#endregion

		#region Public Methods
		#endregion

		#region Protected Methods
		#endregion

		#region Private Methods
		#endregion
	}
}
