﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WordSearch
{
	/// <summary>
	/// This class has been modified form BoardCreator.cs in order for board creation to work on WebGL builds since WebGL does nto support threading.
	/// </summary>
	public class BoardCreatorWebGl : MonoBehaviour
	{
		#region Member Variables

		// The character that the board is filled with at the beginning so we know what elements are empty.
		private const char	BlankChar	= ' ';
		private const int	Tries		= 100;
		
		public BoardConfig			boardConfig;
		public System.Action<Board>	onBoardCompleted;

		private Board							board;
		private System.Random					random;
		private System.Diagnostics.Stopwatch	timer;

		private Board			workingBoard;
		private int				wordIndex;
		private bool			forceStop;

		#endregion

		#region Unity Methods

		private void Start()
		{
			// Before running the algorithm, sort the letters from largest to smallest, this makes more words fit on the board on average
			boardConfig.words.Sort((string x, string y) => { return y.Length - x.Length; });

			random	= new System.Random();
			timer	= new System.Diagnostics.Stopwatch();

			timer.Start();
		}

		private void Update()
		{
			if (forceStop || (workingBoard == null && timer.ElapsedMilliseconds >= boardConfig.algoTimeoutInMilliseconds))
			{
				timer.Stop();

				// Fill the remaining blank spaces with random characters
				for (int i = 0; i < boardConfig.rowSize; i++)
				{
					for (int j = 0; j < boardConfig.columnSize; j++)
					{
						// Check if the board location is blank
						if (board.boardCharacters[i][j] == BlankChar)
						{
							// Create a copy since we modify it
							string	tempRandomCharacters	= boardConfig.randomCharacters;
							bool	characterPlaced			= false;

							// We may need to try placing multiple characters because placing a character could create a word thats in filterWords
							while (tempRandomCharacters.Length > 0)
							{
								// Get a random character
								int		randomCharIndex	= random.Next(0, tempRandomCharacters.Length);
								char	randomCharacter	= tempRandomCharacters[randomCharIndex];

								tempRandomCharacters.Remove(randomCharIndex);

								// Place a random character at the board loaction
								board.boardCharacters[i][j] = randomCharacter;

								// If a filter word was not created break out of the while loop
								if (!CheckForFilterWord(boardConfig, board, new Position(i, j)))
								{
									characterPlaced = true;
									break;
								}

								// Set it back to blank
								board.boardCharacters[i][j] = BlankChar;
							}

							if (!characterPlaced)
							{
								Debug.LogError("[WordSearch] A random character could not be placed because all letters in the randomCharacters list create a word in filterWords.");
							}
						}
					}
				}

				// Call the callback with the completed board and destroy this GameObject as its no longer needed
				onBoardCompleted(board);
				Destroy(gameObject);

				// Done
				return;
			}

			int tries = Tries;

			while (tries > 0)
			{
				tries--;

				if (workingBoard == null)
				{
					// Create a new board to pass to the algorithm
					workingBoard					= new Board();
					workingBoard.rowSize			= boardConfig.rowSize;
					workingBoard.columnSize			= boardConfig.columnSize;
					workingBoard.usedWords			= new List<string>();
					workingBoard.boardCharacters	= new List<List<char>>();
					workingBoard.wordPlacements		= new List<WordPlacement>();

					// Fill the boardCharacters with blank characters
					for (int i = 0; i < boardConfig.rowSize; i++)
					{
						workingBoard.boardCharacters.Add(new List<char>());

						for (int j = 0; j < boardConfig.columnSize; j++)
						{
							workingBoard.boardCharacters[i].Add(BlankChar);
						}
					}

					wordIndex = 0;
				}

				// Get the word we are working on
				string word = boardConfig.words[wordIndex];

				// Check if the word is empty
				if (string.IsNullOrEmpty(word))
				{
					Debug.LogWarning("[WordSearch] One of the words what an empty string.");

					NextWord();

					continue;
				}

				// Try and place the word on the board
				PlaceWordOnBoard(word);
			}
		}

		#endregion

		#region Public Methods

		public static void CreateBoard(BoardConfig boardConfig, System.Action<Board> onBoardCompleted)
		{
			GameObject			boardCreatorWebGlGameObject	= new GameObject("BoardCreatorWebGl");
			BoardCreatorWebGl	boardCreatorWebGl			= boardCreatorWebGlGameObject.AddComponent<BoardCreatorWebGl>();

			boardCreatorWebGl.boardConfig		= boardConfig;
			boardCreatorWebGl.onBoardCompleted	= onBoardCompleted;
		}

		#endregion

		#region Private Methods

		private void PlaceWordOnBoard(string word)
		{
			// Get a list of all the possible indices that the word can start at
			List<Position> possibleIndices = GetPossibleBoardIndices(workingBoard.boardCharacters, word[0]);

			while (possibleIndices.Count > 0)
			{
				// Get a random index to try then remove it from the list of possible indices
				Position startingPosition = possibleIndices[random.Next(0, possibleIndices.Count)];

				possibleIndices.Remove(startingPosition);

				// All the possible direction a word can go.
				// The first index is the verical direction of the word: -1 is up, 1 is down.
				// The second index is the horizontal direction of the word: -1 is left, 1 is right
				List<int[]> possibleDirections = new List<int[]>()
				{
					new int[] {-1, -1},	// Up/Left
					new int[] {-1, 0},	// Up
					new int[] {-1, 1},	// Up/Right
					new int[] {0, 1},	// Right
					new int[] {1, 1},	// Down/Right
					new int[] {1, 0},	// Down
					new int[] {1, -1},	// Down/Left
					new int[] {0, -1}	// Left
				};

				// Try all possible directions
				while (possibleDirections.Count > 0)
				{
					// Get a random direction to try
					int		index		= random.Next(0, possibleDirections.Count);
					int[]	direction	= possibleDirections[index];
					possibleDirections.RemoveAt(index);

					// Get the ending position of the word
					int endRow = startingPosition.row + direction[0] * (word.Length - 1);
					int endCol = startingPosition.col + direction[1] * (word.Length - 1);

					// Check if the ending position is still on the board, if not then continue to choose another direction for the word
					if (endRow < 0 || endRow >= workingBoard.rowSize ||
						endCol < 0 || endCol >= workingBoard.columnSize)
					{
						continue;
					}

					bool canPlace = true;

					// Now check the word does not overlap any letters that it shouldn't overlap
					for (int i = 0; i < word.Length; i++)
					{
						int		row					= startingPosition.row + direction[0] * i;
						int		col					= startingPosition.col + direction[1] * i;
						char	characterOnBoard	= workingBoard.boardCharacters[row][col];

						// If the character on the board is not blank and the characters do not match then this word cannot be placed
						if (characterOnBoard != BlankChar && characterOnBoard != word[i])
						{
							canPlace = false;

							break;
						}
					}

					if (canPlace)
					{
						// Keep a list of positions this word has changed on the board so if we need to remove it we don't remove characters from other words 
						List<Position> placedPositions = new List<Position>();

						// Now we go through each letter of the word and place in on the board by setting the character in boardCharacters
						for (int i = 0; i < word.Length; i++)
						{
							int		row					= startingPosition.row + direction[0] * i;
							int		col					= startingPosition.col + direction[1] * i;
							char	characterOnBoard	= workingBoard.boardCharacters[row][col];

							// If the character on the board is blank then place the words character there, if its not blank then we know
							// that that character is already teh character from the word because of our earlier checks
							if (characterOnBoard == BlankChar)
							{
								workingBoard.boardCharacters[row][col] = word[i];
								placedPositions.Add(new Position(row, col));
							}
						}

						bool foundFilterWord = false;

						// Now that all the characters are placed in boardCharacters lets check if it created any words in the filterWords list
						for (int i = 0; i < word.Length; i++)
						{
							int row = startingPosition.row + direction[0] * i;
							int col = startingPosition.col + direction[1] * i;

							if (CheckForFilterWord(boardConfig, workingBoard, new Position(row, col)))
							{
								foundFilterWord = true;
								break;
							}
						}

						// First check if we found a filter word because if we did then we don't need to check the rest of the board
						if (!foundFilterWord)
						{
							WordPlacement wordPlacement = new WordPlacement();

							wordPlacement.word					= word;
							wordPlacement.startingPosition		= startingPosition;
							wordPlacement.verticalDirection		= direction[0];
							wordPlacement.horizontalDirection	= direction[1];

							workingBoard.wordPlacements.Add(wordPlacement);
							workingBoard.usedWords.Add(word);

							NextWord();

							// We successfully place the word on the board, return so we don't keep trying to place in on the baord
							return;
						}

						// The word created a filter word, need to remove the characters from the board before trying another position/direction
						for (int i = 0; i < placedPositions.Count; i++)
						{
							workingBoard.boardCharacters[placedPositions[i].row][placedPositions[i].col] = BlankChar;
						}
					}
				}
			}
				
			WorkingBoardFinished();
		}

		/// <summary>
		/// Gets a list of all the indicies that a word can begin at
		/// </summary>
		private static List<Position> GetPossibleBoardIndices(List<List<char>> boardCharacters, char startingChar)
		{
			List<Position> possibleIndicies = new List<Position>();

			// Go through each position on the board
			for (int i = 0; i < boardCharacters.Count; i++)
			{
				for (int j = 0; j < boardCharacters[i].Count; j++)
				{
					// If the character is either the blank char or the starting char of the word then add the position
					if (boardCharacters[i][j] == BlankChar || boardCharacters[i][j] == startingChar)
					{
						possibleIndicies.Add(new Position(i, j));
					}
				}
			}

			return possibleIndicies;
		}

		private static bool CheckForFilterWord(BoardConfig boardConfig, Board board, Position position)
		{
			char chararcterAtPosition = board.boardCharacters[position.row][position.col];

			if (chararcterAtPosition == BlankChar)
			{
				return false;
			}

			// Get the list of directions that a word can move in
			List<int[]> directions = new List<int[]>()
			{
				new int[] {-1, -1},	// Up/Left
				new int[] {-1, 0},	// Up
				new int[] {-1, 1},	// Up/Right
				new int[] {0, 1},	// Right
				new int[] {1, 1},	// Down/Right
				new int[] {1, 0},	// Down
				new int[] {1, -1},	// Down/Left
				new int[] {0, -1}	// Left
			};

			// Check each filter word
			for (int i = 0; i < boardConfig.filterWords.Count; i++)
			{
				string	filterWord	= boardConfig.filterWords[i];
				int		charIndex	= -1;

				// We will need to try every character index on filter word for the characterApPosition (so if a word has multiples of the same
				// character, for example filterWord : "butt" has 2 't's in it and we need to check both)
				while (true)
				{
					// Get the index of the next chararcterAtPosition
					charIndex = filterWord.IndexOf(chararcterAtPosition, charIndex + 1);

					// No more characters to check
					if (charIndex == -1)
					{
						break;
					}

					// We need to check for the word in all directions
					foreach (int[] direction in directions)
					{
						// Get where the filter word would start
						int startingRow = position.row - direction[0] * charIndex;
						int startingCol = position.col - direction[1] * charIndex;

						bool found = true;

						// Check if the words match
						for (int j = 0; j < filterWord.Length; j++)
						{
							int row = startingRow + direction[0] * j;
							int col = startingCol + direction[1] * j;

							// If the row/col are out of the boards bounds or the character does not match the filter words character
							if (row < 0 || row >= board.rowSize ||
								col < 0 || col >= board.columnSize ||
								board.boardCharacters[row][col] != filterWord[j])
							{
								found = false;

								break;
							}
						}

						// We found a word to filter!
						if (found)
						{
							return true;
						}
					}
				}
			}

			return false;
		}

		private void NextWord()
		{
			wordIndex++;

			if (wordIndex >= boardConfig.words.Count)
			{
				WorkingBoardFinished();
			}
		}

		private void WorkingBoardFinished()
		{
			// If the tempBoard that was just created has more words on it than the current board then use that board
			if (board == null || board.usedWords.Count < workingBoard.usedWords.Count)
			{
				board = workingBoard;

				// Check if the board has all the words placed on it, if so then we cannot get a better board than this
				if (board.usedWords.Count == boardConfig.words.Count)
				{
					forceStop = true;
				}
			}

			workingBoard = null;
		}

		#endregion
	}
}
