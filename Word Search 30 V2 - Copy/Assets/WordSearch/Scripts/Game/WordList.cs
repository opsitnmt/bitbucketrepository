﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace WordSearch
{
	public class WordList : MonoBehaviour
	{
		#region Inspector Variables

		[SerializeField] private RectTransform	wordListContainer;
		[SerializeField] private WordListItem	wordListItemPrefab;
		[SerializeField] private CanvasGroup	wordListCanvasGroup;
        [SerializeField] private GridLayoutGroup layoutGroup;
        [SerializeField] private RectTransform	sequentialContainer;
		[SerializeField] private WordListItem	sequentialItemPrefab;
		//MO: changed vertical layout group to Grid layout group

		#endregion

		#region Member Variables

		private ObjectPool							wordListItemPool;
		private ObjectPool							sequentialItemPool;
		private Dictionary<string, WordListItem>	wordListItems;
		private List<string>						sequentialWords;
		private string								currentSequentialWord;

		#endregion

		#region Public Methods

		public void Initialize()
		{
			wordListItemPool	= new ObjectPool(wordListItemPrefab.gameObject, 10, wordListContainer);
			sequentialItemPool	= new ObjectPool(sequentialItemPrefab.gameObject, 10, sequentialContainer);
			wordListItems		= new Dictionary<string, WordListItem>();
			sequentialWords		= new List<string>();
            layoutGroup.enabled = false;
            layoutGroup.enabled = true;
            layoutGroup.childAlignment = TextAnchor.UpperCenter;
		}

		public void Setup(Board board)
		{
			Clear();

			// Create a new list since we are going to modify it
			sequentialWords = new List<string>(board.usedWords);

			if (WordSearchController.Instance.IsSequentialMode)
			{
				// If its sequential mode then only add the first word to the sequential word container
				NextSequentialWord(false);
			}
			else
			{
				// Add all the words to the word list container
				for (int i = 0; i < sequentialWords.Count; i++)
				{
					CreateWordListItem(sequentialWords[i], wordListItemPool);
				}
			}

			Tween.CanvasGroupAlpha(wordListCanvasGroup, Tween.TweenStyle.EaseOut, 0f, 1f, 400f);
		}

		public void SetWordFound(string word, bool fromSave)
		{
			if (wordListItems.ContainsKey(word))
			{
				if (WordSearchController.Instance.IsSequentialMode)
				{
					// Only do the animation if SetWordFound was called because the player selected a word
					NextSequentialWord(!fromSave);
				}
				else
				{
					wordListItems[word].foundIndicator.SetActive(true);
				}
			}
			else if (WordSearchController.Instance.IsSequentialMode)
			{
				sequentialWords.Remove(word);
			}
			else
			{
				Debug.LogError("[WordList] Word does not exist in the word list: " + word);
			}
		}

		public void Clear()
		{
			wordListItemPool.ReturnAllObjectsToPool();
			sequentialItemPool.ReturnAllObjectsToPool();
			wordListItems.Clear();
			sequentialWords.Clear();
			currentSequentialWord = "";

			//wordListContainer.sizeDelta	= new Vector2(wordListContainer.sizeDelta.x, 0f);
			wordListCanvasGroup.alpha	= 0f;
		}

		#endregion

		#region Private Methods

		private void NextSequentialWord(bool doAnimation)
		{
			WordListItem currentItem	= string.IsNullOrEmpty(currentSequentialWord) ? null : wordListItems[currentSequentialWord];
			WordListItem nextItem		= sequentialWords.Count == 0 ? null : CreateWordListItem(sequentialWords[0], sequentialItemPool);

			currentSequentialWord = sequentialWords.Count == 0 ? "" : sequentialWords[0];

			if (sequentialWords.Count > 0)
			{
				sequentialWords.RemoveAt(0);
			}

			if (doAnimation)
			{
				float containerWidth = (sequentialContainer.transform as RectTransform).rect.width;

				if (currentItem != null)
				{
					Tween tween = Tween.PositionX(currentItem.transform, Tween.TweenStyle.EaseOut, 0f, -containerWidth, 1000f);

					tween.SetUseRectTransform(true);
					tween.SetFinishCallback((GameObject tweenedObject) => 
					{
						// Return the object back to the pool
						tweenedObject.SetActive(false);
					});
				}

				if (nextItem != null)
				{
					Tween.PositionX(nextItem.transform, Tween.TweenStyle.EaseOut, containerWidth, 0f, 1000f).SetUseRectTransform(true);
				}
			}
			else
			{
				if (currentItem != null)
				{
					currentItem.gameObject.SetActive(false);
				}

				if (nextItem != null)
				{
					(nextItem.transform as RectTransform).anchoredPosition = Vector2.zero;
				}
			}
		}

		private WordListItem CreateWordListItem(string word, ObjectPool itemPool)
		{
			WordListItem wordListItem = null;

			if (!wordListItems.ContainsKey(word))
			{
				wordListItem				= itemPool.GetObject().GetComponent<WordListItem>();
				wordListItem.wordText.text	= word.ToUpper();
				wordListItem.gameObject.SetActive(true);

				if (wordListItem.foundIndicator != null)
				{
					wordListItem.foundIndicator.SetActive(false);
				}

				wordListItems.Add(word, wordListItem);
			}
			else
			{
				Debug.LogWarning("[WordList] Board contains duplicate words. Word: " + word);
			}

			return wordListItem;
		}

		#endregion
	}
}
