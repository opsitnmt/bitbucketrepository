﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WordSearch;

public class OnWordFound : MonoBehaviour {

    public Image panel;
    public Image ShownImage;
    public  Sprite[] Pictures;   
    public  AudioClip[] Sounds;
    public List<TextAsset> textAssets;
    public List<string> words;
    AudioSource auSource;
    public static OnWordFound instance;

    public ForkParticleEffect[] particleEffect;

	// Use this for initialization
	void Start () {
        auSource = GetComponent<AudioSource>();
        //ShownImage.gameObject.SetActive(false);
        instance = this;
        panel.gameObject.SetActive(false);
        WordSearchController.Instance.OnWordFound += CheckWord;
	}

    private void OnValidate()
    {
        //ValidateWords();
    }

    void ValidateWords()
    {
        words.Clear();
        for (int i = 0; i < textAssets.Count; i++)
        {
            string[] str = textAssets[i].text.Split('\n');
            for (int j = 0; j < str.Length; j++)
            {
                words.Add(str[j]);
            }
        }

        for (int i = 0; i < words.Count; i++)
        {
            string temp = "";
            for (int j = 0; j < Pictures.Length; j++)
            {
                string picturename = Pictures[j].name.ToLower();
                //if (!words[i].ToLower().Equals(picturename))
                //    Debug.Log("Name and Word is different " + words[i].ToLower() + " | " + picturename);

                if (words[i].ToLower().Equals(picturename))
                {
                    temp = words[i];
                }
                    
            }
            if (string.IsNullOrEmpty(temp))
                Debug.Log("<color=red>" + words[i] + "</color>");
        }
    }


    void CheckWord()
    {
        StartCoroutine(WordFound());
    }
	
	public IEnumerator WordFound()
    {
        yield return new WaitForSeconds(1f);

        for (int i = 0; i < Pictures.Length; i++)
        {

            if (Pictures[i].name.ToLower().Equals(WordSearchController.Instance.wordThatFound.ToLower()))
            {
                ShownImage.sprite = Pictures[i];
            }
        }

        for (int i = 0; i < Sounds.Length; i++)
        {
            if (Sounds[i].name.ToLower().Equals(WordSearchController.Instance.wordThatFound.ToLower()))
            {
                auSource.clip = Sounds[i];
                duration = Sounds[i].length;
            }
        }
        Debug.Log(WordSearchController.Instance.wordThatFound.ToLower());
        Debug.Log(Pictures.Length);

        panel.gameObject.SetActive(true);
        auSource.Play();
        yield return new WaitForSeconds(auSource.clip.length);

        panel.gameObject.SetActive(false);
        auSource.Stop();
    }

    public float duration;
}
