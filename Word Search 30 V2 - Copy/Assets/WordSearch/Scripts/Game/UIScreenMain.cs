﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace WordSearch
{
	public class UIScreenMain : UIScreen
	{
		#region Inspector Variables
		
		[SerializeField] private Transform		difficultyButtonContainer;
		[SerializeField] private Transform		modeButtonContainer;
		[SerializeField] private OptionButton	optionButtonPrefab;

		[SerializeField] private RectTransform		easyCategoryListContainer;
		[SerializeField] private CategoryListItem	easyCategoryListItemPrefab;
        [SerializeField] private RectTransform      mediumCategoryListContainer;
        [SerializeField] private CategoryListItem   mediumCategoryListItemPrefab;
        [SerializeField] private RectTransform      hardCategoryListContainer;
        [SerializeField] private CategoryListItem   hardCategoryListItemPrefab;
		[SerializeField] private RectTransform      insaneCategoryListContainer;
		[SerializeField] private CategoryListItem   insaneCategoryListItemPrefab;
		[SerializeField] private RectTransform      btn5ListContainer;
		[SerializeField] private CategoryListItem   btn5ListItemPrefab;
		[SerializeField] private RectTransform      btn6ListContainer;
		[SerializeField] private CategoryListItem   btn6ListItemPrefab;
		[SerializeField] private RectTransform      btn7ListContainer;
		[SerializeField] private CategoryListItem   btn7ListItemPrefab;
		[SerializeField] private RectTransform      btn8ListContainer;
		[SerializeField] private CategoryListItem   btn8ListItemPrefab;
		[SerializeField] private RectTransform      btn9ListContainer;
		[SerializeField] private CategoryListItem   btn9ListItemPrefab;
		[SerializeField] private RectTransform      btn10ListContainer;
		[SerializeField] private CategoryListItem   btn10ListItemPrefab;
		[SerializeField] private RectTransform      btn11ListContainer;
		[SerializeField] private CategoryListItem   btn11ListItemPrefab;
		[SerializeField] private RectTransform      btn12ListContainer;
		[SerializeField] private CategoryListItem   btn12ListItemPrefab;


        [SerializeField] private GameObject		continueHeader;
		[SerializeField] private GameObject		continueContent;
		[SerializeField] private CanvasGroup	continueItemCanvasGroup;
		[SerializeField] private Image			continueIconImage;
		[SerializeField] private Text			continueCategoryText;
		[SerializeField] private Text			continueWordsFoundText;
		[SerializeField] private Text			continueTimeLeftText;

		#endregion

		#region Member Variables

		private List<OptionButton>		difficultyButtons;
		private List<OptionButton>		modeButtons;
		private List<CategoryListItem>	categoryListItems;

		#endregion

		#region Public Methods

		public override void Initialize()
		{
			difficultyButtons	= new List<OptionButton>();
			modeButtons			= new List<OptionButton>();
			categoryListItems	= new List<CategoryListItem>();

           // //Create the mode buttons
           // for (int i = 0; i < WordSearchController.Instance.ModeInfos.Count; i++)
           // {
           //     WordSearchController.ModeInfo modeInfo = WordSearchController.Instance.ModeInfos[i];
           //     OptionButton modeButton = Instantiate(optionButtonPrefab);

           //     modeButton.transform.SetParent(modeButtonContainer, false);
           //     modeButton.Setup(modeInfo.modeName, i, OnModeClicked);

           //     modeButtons.Add(modeButton);
           // }

           //// Create the difficulty buttons
           // for (int i = 0; i < WordSearchController.Instance.DifficultyInfos.Count; i++)
           // {
           //     WordSearchController.DifficultyInfo difficultyInfo = WordSearchController.Instance.DifficultyInfos[i];
           //     OptionButton difficultyButton = Instantiate(optionButtonPrefab);

           //     difficultyButton.transform.SetParent(difficultyButtonContainer, false);
           //     difficultyButton.Setup(difficultyInfo.difficultyName, i, OnDifficultyClicked);

           //     difficultyButtons.Add(difficultyButton);
           // }

            // Create a list item for each category and load the words from the words file
            for (int i = 0; i < WordSearchController.Instance.EasyCategoryInfos.Count; i++)
			{
				WordSearchController.CategoryInfo	categoryInfo		= WordSearchController.Instance.EasyCategoryInfos[i];
				CategoryListItem					categoryListItem	= Instantiate(easyCategoryListItemPrefab);

				categoryListItem.transform.SetParent(easyCategoryListContainer, false);
				categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 0, OnCategoryListItemClicked);
				categoryListItems.Add(categoryListItem);
			}

            for (int i = 0; i < WordSearchController.Instance.MeduimCategoryInfos.Count; i++)
            {
                WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.MeduimCategoryInfos[i];
                CategoryListItem categoryListItem = Instantiate(mediumCategoryListItemPrefab);

                categoryListItem.transform.SetParent(mediumCategoryListContainer, false);
                categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 1, OnCategoryListItemClicked);
                
                categoryListItems.Add(categoryListItem);
            }

            for (int i = 0; i < WordSearchController.Instance.HardCategoryInfos.Count; i++)
            {
                WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.HardCategoryInfos[i];
                CategoryListItem categoryListItem = Instantiate(hardCategoryListItemPrefab);

                categoryListItem.transform.SetParent(hardCategoryListContainer, false);
                categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 2, OnCategoryListItemClicked);
                categoryListItems.Add(categoryListItem);
            }

			for (int i = 0; i < WordSearchController.Instance.InsaneCategoryInfos.Count; i++)
			{
				WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.InsaneCategoryInfos[i];
				CategoryListItem categoryListItem = Instantiate(insaneCategoryListItemPrefab);

				categoryListItem.transform.SetParent(insaneCategoryListContainer, false);
				categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 3, OnCategoryListItemClicked);
				categoryListItems.Add(categoryListItem);
			}

			for (int i = 0; i < WordSearchController.Instance.Btn6Infos.Count; i++)
			{
				WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.Btn6Infos[i];
				CategoryListItem categoryListItem = Instantiate(btn6ListItemPrefab);

				categoryListItem.transform.SetParent(btn6ListContainer, false);
				categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 4, OnCategoryListItemClicked);
				categoryListItems.Add(categoryListItem);
			}

			for (int i = 0; i < WordSearchController.Instance.Btn5Infos.Count; i++)
			{
				WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.Btn5Infos[i];
				CategoryListItem categoryListItem = Instantiate(btn5ListItemPrefab);

				categoryListItem.transform.SetParent(btn5ListContainer, false);
				categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 5, OnCategoryListItemClicked);
				categoryListItems.Add(categoryListItem);
			}

			for (int i = 0; i < WordSearchController.Instance.Btn7Infos.Count; i++)
			{
				WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.Btn7Infos[i];
				CategoryListItem categoryListItem = Instantiate(btn7ListItemPrefab);

				categoryListItem.transform.SetParent(btn7ListContainer, false);
				categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 6, OnCategoryListItemClicked);
				categoryListItems.Add(categoryListItem);
			}

			for (int i = 0; i < WordSearchController.Instance.Btn8Infos.Count; i++)
			{
				WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.Btn8Infos[i];
				CategoryListItem categoryListItem = Instantiate(btn8ListItemPrefab);

				categoryListItem.transform.SetParent(btn8ListContainer, false);
				categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 7, OnCategoryListItemClicked);
				categoryListItems.Add(categoryListItem);
			}

			for (int i = 0; i < WordSearchController.Instance.Btn9Infos.Count; i++)
			{
				WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.Btn9Infos[i];
				CategoryListItem categoryListItem = Instantiate(btn9ListItemPrefab);

				categoryListItem.transform.SetParent(btn9ListContainer, false);
				categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 8, OnCategoryListItemClicked);
				categoryListItems.Add(categoryListItem);
			}

			for (int i = 0; i < WordSearchController.Instance.Btn10Infos.Count; i++)
			{
				WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.Btn10Infos[i];
				CategoryListItem categoryListItem = Instantiate(btn10ListItemPrefab);

				categoryListItem.transform.SetParent(btn10ListContainer, false);
				categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 9, OnCategoryListItemClicked);
				categoryListItems.Add(categoryListItem);
			}

			for (int i = 0; i < WordSearchController.Instance.Btn11Infos.Count; i++)
			{
				WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.Btn11Infos[i];
				CategoryListItem categoryListItem = Instantiate(btn11ListItemPrefab);

				categoryListItem.transform.SetParent(btn11ListContainer, false);
				categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 10, OnCategoryListItemClicked);
				categoryListItems.Add(categoryListItem);
			}

			for (int i = 0; i < WordSearchController.Instance.Btn12Infos.Count; i++)
			{
				WordSearchController.CategoryInfo categoryInfo = WordSearchController.Instance.Btn12Infos[i];
				CategoryListItem categoryListItem = Instantiate(btn12ListItemPrefab);

				categoryListItem.transform.SetParent(btn12ListContainer, false);
				categoryListItem.Setup(categoryInfo.categoryName, categoryInfo.bgImage, i, categoryInfo, 11, OnCategoryListItemClicked);
				categoryListItems.Add(categoryListItem);
			}

            SetButtonsSelected(modeButtons, WordSearchController.Instance.ModeInfos.IndexOf(WordSearchController.Instance.SelectedMode));
			SetButtonsSelected(difficultyButtons, WordSearchController.Instance.DifficultyInfos.IndexOf(WordSearchController.Instance.SelectedDifficulty));
		}

		public override void OnShowing(string data)
		{
			easyCategoryListContainer.anchoredPosition = new Vector2(easyCategoryListContainer.anchoredPosition.x, 0f);

            for (int i = 0; i < categoryListItems.Count; i++)
            {
                categoryListItems[i].StartShowAnimation(0.2f + i * 0.05f);
            }

            for (int i = 0; i < difficultyButtons.Count; i++)
            {
                difficultyButtons[i].StartShowAnimation(0.2f + i * 0.05f);
            }

            for (int i = 0; i < modeButtons.Count; i++)
            {
                modeButtons[i].StartShowAnimation(0.2f + i * 0.05f);
            }

            //bool isBoardActive = (WordSearchController.Instance.ActiveBoardState == WordSearchController.BoardState.BoardActive);

            //continueHeader.SetActive(isBoardActive);
            //continueContent.SetActive(isBoardActive);

            //         if (isBoardActive)
            //         {
            //             continueIconImage.sprite = WordSearchController.Instance.ActiveCategory.categoryIcon;
            //             continueCategoryText.text = WordSearchController.Instance.ActiveCategory.categoryName;
            //             continueWordsFoundText.text = string.Format("Words Found: {0}/{1}", WordSearchController.Instance.FoundWords.Count, WordSearchController.Instance.ActiveBoard.usedWords.Count);

            //             continueTimeLeftText.gameObject.SetActive(WordSearchController.Instance.IsTimedMode);

            //             if (WordSearchController.Instance.IsTimedMode)
            //             {
            //                 int minutesLeft = Mathf.FloorToInt((float)WordSearchController.Instance.TimeLeftInSeconds / 60f);
            //                 int secondsLeft = WordSearchController.Instance.TimeLeftInSeconds % 60;

            //                 continueTimeLeftText.text = string.Format("Time Left: {0}:{1}{2}", minutesLeft, (secondsLeft < 10 ? "0" : ""), secondsLeft);
            //             }

            //             continueItemCanvasGroup.alpha = 0f;

            //             StartCoroutine(WaitThenAnimateContinueItem(0.2f));
            //         }
        }

		public void OnContinueButtonClicked()
		{
			// All the Active properties should be setup properly, just show the game screen to continue
			UIScreenController.Instance.Show("game");
		}

        #endregion

        #region Private Methods

		private void OnCategoryListItemClicked(int index)
		{
			// Setup the game using the category that was clicked
			//WordSearchController.Instance.StartCategory(index);

            // Show the game screen
            UIScreenController.Instance.Show("game");
		}


		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//		WordSearchController.CategoryInfo categoryInfos;
//		int timer = 0;
//		int difIndex;
//		void Update(){
//
//			timer++;
//			if (timer == 300) {
//				//WordSearchController.Instance.StartCategory(WordSearchController.Instance.ActiveCategory);
//				//WordSearchController.StartCategory(int categoryIndex, List<CategoryInfo> categoryInfo);
//				//CategoryListItem.SetCategoryInfo();
//				WordSearchController.Instance.SetSelectedDifficulty(difIndex);
//				WordSearchController.Instance.SetSelectedMode(0);
//
//				UIScreenController.Instance.Show("game");
//			}
//
//
//		}

		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		private void OnModeClicked(int index)
		{
			SetButtonsSelected(modeButtons, index);

			WordSearchController.Instance.SetSelectedMode(index);
		}

		private void OnDifficultyClicked(int index)
		{
			SetButtonsSelected(difficultyButtons, index);

			WordSearchController.Instance.SetSelectedDifficulty(index);
		}

		private void SetButtonsSelected(List<OptionButton> optionButtons, int selectedButtonIndex)
		{
			for (int i = 0; i < optionButtons.Count; i++)
			{
				optionButtons[i].IsSelected = (i == selectedButtonIndex);
			}
		}

		private IEnumerator WaitThenAnimateContinueItem(float delay)
		{
			yield return new WaitForSeconds(delay);

			Tween.CanvasGroupAlpha(continueItemCanvasGroup, Tween.TweenStyle.EaseOut, 0f, 1f, 400f);
		}

		#endregion
	}
}