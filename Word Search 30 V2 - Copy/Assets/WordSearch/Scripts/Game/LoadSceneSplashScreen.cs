﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class LoadSceneSplashScreen : MonoBehaviour {

    public VideoClip videoClip;
	public GameObject LoadScreen;

	// Use this for initialization
	void Start () {
        StartCoroutine(LoadSceneCo());
		LoadScreen.SetActive (false);
	}
	
	IEnumerator LoadSceneCo()
    {
        yield return new WaitForSeconds((float)videoClip.length);  
		LoadScreen.SetActive (true);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
