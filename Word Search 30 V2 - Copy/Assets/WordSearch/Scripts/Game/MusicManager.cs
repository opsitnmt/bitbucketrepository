﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicManager : MonoBehaviour {

    public static MusicManager instance;
    public List<AudioClip> bgMusics;
    internal AudioSource audioSource;
    int musicIndex;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        PlayMusic();
        Mute();
        instance = this;
    }

    void PlayMusic()
    {
        StartCoroutine(PlayMusicCo());
    }

    IEnumerator PlayMusicCo()
    {
        if (musicIndex > bgMusics.Count - 1)
            musicIndex = 0;

        audioSource.clip = bgMusics[musicIndex];
        audioSource.Play();
        Debug.Log(bgMusics[musicIndex].length);
        yield return new WaitForSeconds(bgMusics[musicIndex].length);
        musicIndex++;
        PlayMusic();
        Debug.Log(bgMusics[musicIndex].name);
    }

    bool isMusicOn = false;
    public Text muteText;
    public Button muteButton;
    public void Mute()
    {
        isMusicOn = !isMusicOn;
        if(isMusicOn)
        {
            muteText.text = "Music On";
            var buttonColors = muteButton.colors;
            buttonColors.normalColor = Color.green;
            muteButton.colors = buttonColors;
            AudioListener.volume = 1;
        }else
        {
            muteText.text = "Music Off";
            var buttonColors = muteButton.colors;
            buttonColors.normalColor = Color.red;
            muteButton.colors = buttonColors;
            AudioListener.volume = 0;
        }
    }
}
