﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace WordSearch
{
	public class OptionButton : Button
	{
		#region Inspector Variables

		public Text		uiText;

		public Color	textNormalColor			= Color.white;
		public Color	textHighlightedColor	= Color.white;
		public Color	textPressedColor		= Color.white;
		public Color	textDisabledColor		= Color.white;

		public Color	bkgSelectedColor	= Color.white;
		public Color	bkgNormalColor		= Color.white;
		public Color	textSelectedColor	= Color.white;

		public CanvasGroup canvasGroup;

		#endregion

		#region Member Variables

		private int					index;
		private System.Action<int>	onOptionClicked;
		private bool				isSelected;

		#endregion

		#region Properties

		public bool IsSelected
		{
			get
			{
				return isSelected;
			}

			set
			{
				isSelected = value;

				ColorBlock cb	= colors;
				cb.normalColor	= isSelected ? bkgSelectedColor : bkgNormalColor;
				colors			= cb;
			}
		}

		#endregion

		#region Unity Methods

		public void Update()
		{
			if (uiText == null)
			{
				return;
			}

			if (!interactable)
			{
				uiText.color = textDisabledColor;
			}
			else if (currentSelectionState == SelectionState.Normal)
			{
				uiText.color = isSelected ? textSelectedColor : textNormalColor;
			}
			else if (currentSelectionState == SelectionState.Highlighted)
			{
				uiText.color = textHighlightedColor;
			}
			else if (currentSelectionState == SelectionState.Pressed)
			{
				uiText.color = textPressedColor;
			}
			else if (currentSelectionState == SelectionState.Disabled)
			{
				uiText.color = textDisabledColor;
			}
		}

		#endregion

		#region Public Methods

		public void Setup(string difficultyName, int index, System.Action<int> onToggleChanged)
		{
			this.index				= index;
			this.onOptionClicked	= onToggleChanged;

			uiText.text = difficultyName;
		}

		public void OnClicked()
		{
			if (onOptionClicked != null)
			{
				onOptionClicked(index);
			}
		}

		public void StartShowAnimation(float delay)
		{
			if (canvasGroup != null)
			{
				canvasGroup.alpha = 0f;

				if (delay == 0)
				{
					StartShowAnimation();
				}
				else
				{
					StartCoroutine(WaitThenStartShowAnimation(delay));
				}
			}
		}

		#endregion

		#region Private Methods

		private IEnumerator WaitThenStartShowAnimation(float delay)
		{
			yield return new WaitForSeconds(delay);

			StartShowAnimation();
		}

		private void StartShowAnimation()
		{
			Tween.CanvasGroupAlpha(canvasGroup, Tween.TweenStyle.EaseOut, 0f, 1f, 400f);
		}

		#endregion
	}
}
