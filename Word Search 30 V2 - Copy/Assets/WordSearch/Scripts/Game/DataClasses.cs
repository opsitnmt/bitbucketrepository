﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WordSearch
{
	public class Board
	{
		public int					rowSize;
		public int					columnSize;
		public List<string>			usedWords;
		public List<List<char>>		boardCharacters;
		public List<WordPlacement>	wordPlacements;

		public Board Copy()
		{
			Board board = new Board();

			board.rowSize			= rowSize;
			board.columnSize		= columnSize;
			board.usedWords			= new List<string>(usedWords);
			board.wordPlacements	= new List<WordPlacement>(wordPlacements);

			board.boardCharacters = new List<List<char>>();

			for (int i = 0; i < boardCharacters.Count; i++)
			{
				board.boardCharacters.Add(new List<char>(boardCharacters[i]));
			}

			return board;
		}
	}

	public class BoardConfig
	{
		public int				rowSize;
		public int				columnSize;
		public List<string>		words;
		public List<string>		filterWords;
		public string			randomCharacters			= "abcdefghijklmnopqrstuvwxyz";
		public long				algoTimeoutInMilliseconds	= 2000;	// If this is 0 then there will be no timeout, the algorithm will run till it places all words of fails to place all words
	}

	public class WordPlacement
	{
		public string	word;
		public Position	startingPosition;
		public int		verticalDirection;
		public int		horizontalDirection;
	}

	public class Position
	{
		public int row;
		public int col;

		public Position(int row, int col)
		{
			this.row = row;
			this.col = col;
		}
	}
}
