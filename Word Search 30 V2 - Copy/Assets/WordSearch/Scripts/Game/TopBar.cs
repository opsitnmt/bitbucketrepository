﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

namespace WordSearch
{
	public class TopBar : SingletonComponent<TopBar>
	{
		#region Inspector Variables

		[SerializeField] private RectTransform backButton;
		[SerializeField] private RectTransform mainScreenContent;
		//[SerializeField] private RectTransform gameScreenContent;

		[SerializeField] private Image	categoryIconImage;
		[SerializeField] private Text	categoryNameText;
		[SerializeField] private Text	wordsFoundText;
		[SerializeField] private Text	timeLeftText;

		#endregion

		#region Unity Methods

		private void Start()
		{
			UIScreenController.Instance.OnSwitchingScreens += OnSwitchingScreens;

			WordSearchController.Instance.OnBoardStateChanged	+= UpdateGameUI;
			WordSearchController.Instance.OnWordFound			+= UpdateGameUI;

			UpdateGameUI();
		}

		private void Update()
		{
			if (WordSearchController.Instance.IsTimedMode)
			{
				int minutesLeft = Mathf.FloorToInt((float)WordSearchController.Instance.TimeLeftInSeconds / 60f);
				int secondsLeft = WordSearchController.Instance.TimeLeftInSeconds % 60;

				timeLeftText.text = string.Format("{0}:{1}{2}", minutesLeft, (secondsLeft < 10 ? "0" : ""), secondsLeft);
			}
		}

		#endregion

		#region Public Methods

		public void OnBackButtonClicked()
		{
			UIScreenController.Instance.Show(UIScreenController.MainScreenId, true);
		}

		#endregion

		#region Private Methods

		private void OnSwitchingScreens(string fromScreenId, string toScreenId, bool overlay)
		{
			// Make sure they are both active
			mainScreenContent.gameObject.SetActive(true);
			//gameScreenContent.gameObject.SetActive(true);

			float animDuration			= UIScreenController.Instance.AnimationSpeed;
			float backButtonOnScreenX	= 48f;
			float backButtonOffScreenX	= -150f;
			float mainContentOffScreenY = 300f;
			float gameContentOffScreenX = 1200f;

			//if (fromScreenId == UIScreenController.MainScreenId && toScreenId == UIScreenController.GameScreenId)
			//{
			//	//Tween.PositionX(backButton, Tween.TweenStyle.EaseOut, backButtonOffScreenX, backButtonOnScreenX, animDuration).SetUseRectTransform(true);
			//	//Tween.PositionY(mainScreenContent, Tween.TweenStyle.EaseOut, 0f, mainContentOffScreenY, animDuration).SetUseRectTransform(true);
			//	//Tween.PositionX(gameScreenContent, Tween.TweenStyle.EaseOut, gameContentOffScreenX, 0f, animDuration).SetUseRectTransform(true);
			//}
			//else if (fromScreenId == UIScreenController.GameScreenId && toScreenId == UIScreenController.MainScreenId)
			//{
			//	Tween.PositionX(backButton, Tween.TweenStyle.EaseOut, backButtonOnScreenX, backButtonOffScreenX, animDuration).SetUseRectTransform(true);
			//	Tween.PositionY(mainScreenContent, Tween.TweenStyle.EaseOut, mainContentOffScreenY, 0f, animDuration).SetUseRectTransform(true);
			//	//Tween.PositionX(gameScreenContent, Tween.TweenStyle.EaseOut, 0f, gameContentOffScreenX, animDuration).SetUseRectTransform(true);
			//}
			//else
			//{
			//	//backButton.anchoredPosition			= new Vector2(backButtonOffScreenX, 0f);
			//	mainScreenContent.anchoredPosition	= Vector2.zero;
			//	//gameScreenContent.anchoredPosition	= new Vector2(gameContentOffScreenX, 0f);
			//}
		}

		private void UpdateGameUI()
		{
			wordsFoundText.text = (WordSearchController.Instance.ActiveBoardState == WordSearchController.BoardState.Generating) ? "Loading Board" : "";

			if (WordSearchController.Instance.ActiveCategory != null)
			{
				categoryIconImage.sprite	= WordSearchController.Instance.ActiveCategory.bgImage;
				categoryNameText.text		= WordSearchController.Instance.ActiveCategory.categoryName;
			}

			if (WordSearchController.Instance.ActiveBoard != null)
			{
				wordsFoundText.text = string.Format("Found: {0}/{1}", WordSearchController.Instance.FoundWords.Count, WordSearchController.Instance.ActiveBoard.usedWords.Count);
			}

			timeLeftText.gameObject.SetActive(WordSearchController.Instance.IsTimedMode);
		}

		#endregion
	}
}
