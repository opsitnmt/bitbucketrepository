﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

namespace WordSearch
{
	public class CharacterGrid : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
	{
		#region Enums

		private enum HighlightType
		{
			Random,
			Range,
			Custom
		}

		#endregion

		#region Inspector Variables

		[SerializeField] private float		maxCellSize;
		[SerializeField] private Vector2	spacing;
		[SerializeField] private RectOffset	padding;

		[SerializeField] private Font 		letterFont;
		[SerializeField] private int 		letterFontSize;
		[SerializeField] private Color		letterColor;
		[SerializeField] private Vector2	letterOffsetInCell;

		[SerializeField] private Sprite			highlightSprite;
		[SerializeField] private float			highlightExtraSize;
		[SerializeField] private float			highlightAlpha;
		[SerializeField] private HighlightType	highlightType;

		// Settings for HighlightType.Range
		[SerializeField] [Range(0, 255)] private int redMin;
		[SerializeField] [Range(0, 255)] private int redMax		= 255;
		[SerializeField] [Range(0, 255)] private int greenMin;
		[SerializeField] [Range(0, 255)] private int greenMax	= 255;
		[SerializeField] [Range(0, 255)] private int blueMin;
		[SerializeField] [Range(0, 255)] private int blueMax	= 255;

		// Settings for HighlightType.Custom
		[SerializeField] private List<Color> customColors;

		#endregion

		#region Member Variables

		private Board currentBoard;

		private RectTransform					gridContainer;
		private RectTransform					gridOverlayContainer;
		private ObjectPool						characterPool;
		private List<List<CharacterGridItem>>	characterItems;
		private List<Image>						highlights;

		private float currentScale;
		private float currentCellSize;

		// Used when the player is selecting a word
		private Image				selectingHighlight;
		private bool				isSelecting;
		private CharacterGridItem	startCharacter;
		private CharacterGridItem	lastEndCharacter;

		#endregion

		#region Properties

		private Vector2	ScaledSpacing				{ get { return spacing * currentScale; } }
		private float	ScaledHighlighExtraSize 	{ get { return highlightExtraSize * currentScale; } }
		private Vector2	ScaledLetterOffsetInCell	{ get { return letterOffsetInCell * currentScale; } }
		private float	CellFullWidth				{ get { return currentCellSize + ScaledSpacing.x; } }
		private float	CellFullHeight				{ get { return currentCellSize + ScaledSpacing.y; } }

		#endregion

		#region Unity Methods

		public void OnPointerDown(PointerEventData eventData)
		{
			if (WordSearchController.Instance.ActiveBoardState == WordSearchController.BoardState.BoardActive)
			{
				// Get the closest word to select
				CharacterGridItem characterItem = GetCharacterItemAtPosition(eventData.position);

				if (characterItem != null)
				{
					// Start selecting
					isSelecting			= true;
					startCharacter		= characterItem;
					lastEndCharacter	= characterItem;

					AssignHighlighColor(selectingHighlight);
					selectingHighlight.gameObject.SetActive(true);

					UpdateSelectingHighlight(eventData.position);
				}
			}
		}

		public void OnDrag(PointerEventData eventData)
		{
			if (WordSearchController.Instance.ActiveBoardState == WordSearchController.BoardState.BoardActive)
			{
				UpdateSelectingHighlight(eventData.position);
			}
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			if (startCharacter != null && lastEndCharacter != null && WordSearchController.Instance.ActiveBoardState == WordSearchController.BoardState.BoardActive)
			{
				// Get the start and end row/col position for the word
				Position wordStartPosition	= new Position(startCharacter.Row, startCharacter.Col);
				Position wordEndPosition	= new Position(lastEndCharacter.Row, lastEndCharacter.Col);

				// Call OnWordSelected to notify the WordSearchController that a word has been selected
				string foundWord = WordSearchController.Instance.OnWordSelected(wordStartPosition, wordEndPosition);

				// If the word was a word that was suppose to be found then highligh the word and create the floating text
				if (!string.IsNullOrEmpty(foundWord))
				{
					HighlightWord(wordStartPosition, wordEndPosition, true);

					// Create the floating text in the middle of the highlighted word
					Vector2 startPosition	= (startCharacter.transform as RectTransform).anchoredPosition;
					Vector2 endPosition		= (lastEndCharacter.transform as RectTransform).anchoredPosition;
					Vector2 center			= endPosition + (startPosition - endPosition) / 2f;

					Text floatingText = CreateFloatingText(foundWord, selectingHighlight.color, center);

					Color toColor = new Color(floatingText.color.r, floatingText.color.g, floatingText.color.b, 0f);

					Tween.PositionY(floatingText.transform, Tween.TweenStyle.Linear, center.y, center.y + 75f, 1000f).SetUseRectTransform(true);
					Tween.Colour(floatingText, Tween.TweenStyle.Linear, floatingText.color, toColor, 1000f).SetFinishCallback((GameObject tweenedObject) => 
					{
						GameObject.Destroy(tweenedObject);
					});
				}
			}

			// End selecting and hide the select highlight
			isSelecting		= false;
			startCharacter	= null;
			selectingHighlight.gameObject.SetActive(false);
		}

		#endregion

		#region Public Methods

		public void Initialize()
		{
			// Create a new GameObject to hold all the letters, set it as a child of CharacterGrid and set its anchors to expand to fill
			GameObject gridContainerObject	= new GameObject("grid_container", typeof(RectTransform), typeof(GridLayoutGroup), typeof(CanvasGroup));
			gridContainer					= gridContainerObject.GetComponent<RectTransform>();
			gridContainer.SetParent(transform, false);
			gridContainer.anchoredPosition	= Vector2.zero;
			gridContainer.anchorMin			= Vector2.zero;
			gridContainer.anchorMax			= Vector2.one;
			gridContainer.offsetMin			= Vector2.zero;
			gridContainer.offsetMax			= Vector2.zero;

			// Create another GameObject that will be be used to place things overtop of the letter grid (like highlights)
			GameObject gridOverlayContainerObject	= new GameObject("grid_overlay_container", typeof(RectTransform));
			gridOverlayContainer					= gridOverlayContainerObject.GetComponent<RectTransform>();
			gridOverlayContainer.SetParent(transform, false);
			gridOverlayContainer.anchoredPosition	= Vector2.zero;
			gridOverlayContainer.anchorMin			= Vector2.zero;
			gridOverlayContainer.anchorMax			= Vector2.one;
			gridOverlayContainer.offsetMin			= Vector2.zero;
			gridOverlayContainer.offsetMax			= Vector2.zero;

			// Create a CharacterGridItem that will be used as a template by the ObjectPool to create more instance
			CharacterGridItem templateCharacterGridItem = CreateCharacterGridItem();
			templateCharacterGridItem.name = "template_character_grid_item";
			templateCharacterGridItem.gameObject.SetActive(false);
			templateCharacterGridItem.transform.SetParent(transform, false);

			GameObject characterPoolContainer = new GameObject("character_pool");
			characterPoolContainer.transform.SetParent(transform);
			characterPoolContainer.SetActive(false);

			characterPool	= new ObjectPool(templateCharacterGridItem.gameObject, 25, characterPoolContainer.transform);
			characterItems	= new List<List<CharacterGridItem>>();
			highlights		= new List<Image>();

			// Instantiate an instance of the highlight to use when the player is selecting a word
			selectingHighlight = CreateNewHighlight();
			selectingHighlight.gameObject.SetActive(false);
		}

		public void Setup(Board board)
		{
			Clear();

			// We want to scale the CharacterItem so that the UI Text changes size
			currentCellSize	= SetupGridContainer(board.rowSize, board.columnSize);
			currentScale	= currentCellSize / maxCellSize;

			for (int i = 0; i < board.boardCharacters.Count; i++)
			{
				characterItems.Add(new List<CharacterGridItem>());

				for (int j = 0; j < board.boardCharacters[i].Count; j++)
				{
					// Get a new character from the object pool
					CharacterGridItem characterItem = characterPool.GetObject().GetComponent<CharacterGridItem>();

					characterItem.Row = i;
					characterItem.Col = j;

					characterItem.gameObject.SetActive(true);
					characterItem.transform.SetParent(gridContainer, false);

					characterItem.characterText.text					= board.boardCharacters[i][j].ToString().ToUpper();
					characterItem.characterText.transform.localScale	= new Vector3(currentScale, currentScale, 1f);

					(characterItem.characterText.transform as RectTransform).anchoredPosition = ScaledLetterOffsetInCell;

					characterItems[i].Add(characterItem);
				}
			}

			currentBoard = board;

			Tween.CanvasGroupAlpha(gridContainer.GetComponent<CanvasGroup>(), Tween.TweenStyle.EaseOut, 0f, 1f, 400f);
		}

		public void HighlightWord(Position start, Position end, bool useSelectedColour)
		{
			Image highlight = CreateNewHighlight();

			highlights.Add(highlight);

			PositionHighlight(highlight, characterItems[start.row][start.col], characterItems[end.row][end.col]);

			if (useSelectedColour && selectingHighlight != null)
			{
				highlight.color = selectingHighlight.color;
			}
		}

		public void Clear()
		{
			characterPool.ReturnAllObjectsToPool();
			characterItems.Clear();

			for (int i = 0; i < highlights.Count; i++)
			{
				Destroy(highlights[i].gameObject);
			}

			highlights.Clear();

			gridContainer.GetComponent<CanvasGroup>().alpha = 0f;
		}

		#endregion

		#region Private Methods

		private void UpdateSelectingHighlight(Vector2 screenPosition)
		{
			if (isSelecting)
			{
				CharacterGridItem endCharacter = GetCharacterItemAtPosition(screenPosition);

				// If endCharacter is null then the mouse position must be off the grid container
				if (endCharacter != null)
				{
					int startRow = startCharacter.Row;
					int startCol = startCharacter.Col;

					int endRow = endCharacter.Row;
					int endCol = endCharacter.Col;

					int rowDiff = endRow - startRow;
					int colDiff	= endCol - startCol;

					// Check to see if the line from the start to the end is not vertical/horizontal/diagonal
					if (rowDiff != colDiff && rowDiff != 0 && colDiff != 0)
					{
						// Now we will find the best new end character position. All code below makes the highlight snap to a proper vertical/horizontal/diagonal line
						if (Mathf.Abs(colDiff) > Mathf.Abs(rowDiff))
						{
							if (Mathf.Abs(colDiff) - Mathf.Abs(rowDiff) > Mathf.Abs(rowDiff))
							{
								rowDiff = 0;
							}
							else
							{
								colDiff = AssignKeepSign(colDiff, rowDiff);
							}
						}
						else
						{
							if (Mathf.Abs(rowDiff) - Mathf.Abs(colDiff) > Mathf.Abs(colDiff))
							{
								colDiff = 0;
							}
							else
							{
								colDiff = AssignKeepSign(colDiff, rowDiff);
							}
						}

						if (startCol + colDiff < 0)
						{
							colDiff = colDiff - (startCol + colDiff);
							rowDiff = AssignKeepSign(rowDiff, Mathf.Abs(colDiff));
						}
						else if (startCol + colDiff >= currentBoard.columnSize)
						{
							colDiff = colDiff - (startCol + colDiff - currentBoard.columnSize + 1);
							rowDiff = AssignKeepSign(rowDiff, Mathf.Abs(colDiff));
						}

						endCharacter = characterItems[startRow + rowDiff][startCol + colDiff];
					}
				}
				else
				{
					// Use the last selected end character
					endCharacter = lastEndCharacter;
				}

				// Position the select highlight in the proper position
				PositionHighlight(selectingHighlight, startCharacter, endCharacter);

				// Set the last end character so if the player drags outside the grid container then we have somewhere to drag to
				lastEndCharacter = endCharacter;
			}
		}

		private void PositionHighlight(Image highlight, CharacterGridItem start, CharacterGridItem end)
		{
			RectTransform	highlightRectT	= highlight.transform as RectTransform;
			Vector2			startPosition	= (start.transform as RectTransform).anchoredPosition;
			Vector2			endPosition		= (end.transform as RectTransform).anchoredPosition;

			float distance			= Vector2.Distance(startPosition, endPosition);
			float highlightWidth	= currentCellSize + distance + ScaledHighlighExtraSize;
			float highlightHeight	= currentCellSize + ScaledHighlighExtraSize;
			float scale				= highlightHeight / highlight.sprite.rect.height;

			// Set position and size
			highlightRectT.anchoredPosition	= startPosition + (endPosition - startPosition) / 2f;

			// Now Set the size of the highlight
			highlightRectT.localScale	= new Vector3(scale, scale);
			highlightRectT.sizeDelta	= new Vector2(highlightWidth / scale, highlight.sprite.rect.height);

			// Set angle
			float angle = Vector2.Angle(new Vector2(1f, 0f), endPosition - startPosition);

			if (startPosition.y > endPosition.y)
			{
				angle = -angle;
			}

			highlightRectT.eulerAngles = new Vector3(0f, 0f, angle);
		}

		private CharacterGridItem GetCharacterItemAtPosition(Vector2 screenPoint)
		{
			for (int i = 0; i < characterItems.Count; i++)
			{
				for (int j = 0; j < characterItems[i].Count; j++)
				{
					Vector2 localPoint;

					RectTransformUtility.ScreenPointToLocalPointInRectangle(characterItems[i][j].transform as RectTransform, screenPoint, null, out localPoint);

					// Check if the localPoint is inside the cell in the grid
					localPoint.x += CellFullWidth / 2f;
					localPoint.y += CellFullHeight / 2f;

					if (localPoint.x >= 0 && localPoint.y >= 0 && localPoint.x < CellFullWidth && localPoint.y < CellFullHeight)
					{
						return characterItems[i][j];
					}
				}
			}

			return null;
		}

		private float SetupGridContainer(int rows, int columns)
		{
			// Add a GridLayoutGroup so make positioning letters much easier
			GridLayoutGroup	gridLayoutGroup = gridContainer.GetComponent<GridLayoutGroup>();

			// Get the width and height of a cell
			float cellWidth		= (gridContainer.rect.width - ScaledSpacing.x * (columns - 1) - padding.horizontal) / columns;
			float cellHeight	= (gridContainer.rect.height - ScaledSpacing.y * (rows - 1) - padding.vertical) / rows;
			float cellSize		= Mathf.Min(cellWidth, cellHeight, maxCellSize);

			gridLayoutGroup.cellSize		= new Vector2(cellSize, cellSize);
			gridLayoutGroup.spacing			= ScaledSpacing;
			gridLayoutGroup.padding			= padding;
			gridLayoutGroup.childAlignment	= TextAnchor.MiddleCenter;
			gridLayoutGroup.constraint		= GridLayoutGroup.Constraint.FixedColumnCount;
			gridLayoutGroup.constraintCount	= columns;

			return cellSize;
		}

		private CharacterGridItem CreateCharacterGridItem()
		{
			GameObject characterGridItemObject	= new GameObject("character_grid_item", typeof(RectTransform));
			GameObject textObject				= new GameObject("character_text", typeof(RectTransform));

			// Set the text object as a child of the CharacterGridItem object and set its position as the offset
			textObject.transform.SetParent(characterGridItemObject.transform);
			(textObject.transform as RectTransform).anchoredPosition = letterOffsetInCell;

			// Add the Text component for the item and set the font/fontSize
			Text characterText 		= textObject.AddComponent<Text>();
			characterText.font 		= letterFont;
			characterText.fontSize	= letterFontSize;
			characterText.color		= letterColor;

			// Create a ContentSizeFitter for the text object so the size will always fit the letter in it
			ContentSizeFitter textCSF	= textObject.AddComponent<ContentSizeFitter>();
			textCSF.horizontalFit		= ContentSizeFitter.FitMode.PreferredSize;
			textCSF.verticalFit			= ContentSizeFitter.FitMode.PreferredSize;

			// Add the CharacterGridItem component
			CharacterGridItem characterGridItem	= characterGridItemObject.AddComponent<CharacterGridItem>();
			characterGridItem.characterText		= characterText;
            characterText.raycastTarget = false;

            //Add an empty image for raycas target
            Image bgImage = characterGridItemObject.AddComponent<Image>();
            Color bgColor = bgImage.color;
            bgColor.a = 0;
            bgImage.color = bgColor;

			return characterGridItem;
		}

		private Image CreateNewHighlight()
		{
			GameObject		highlightObject	= new GameObject("highlight");
			RectTransform	highlightRectT	= highlightObject.AddComponent<RectTransform>();
			Image			highlightImage	= highlightObject.AddComponent<Image>();

			highlightRectT.anchorMin = new Vector2(0f, 1f);
			highlightRectT.anchorMax = new Vector2(0f, 1f);
			highlightRectT.SetParent(gridOverlayContainer, false);

			highlightImage.type			= Image.Type.Sliced;
			highlightImage.fillCenter	= true;
			highlightImage.sprite		= highlightSprite;

			AssignHighlighColor(highlightImage);

			if (selectingHighlight != null)
			{
				// Set the selected highlight as the last sibling so that it will always be drawn ontop of all other highlights
				selectingHighlight.transform.SetAsLastSibling();
			}

			return highlightImage;
		}

		private void AssignHighlighColor(Image highlight)
		{
			Color color = Color.white;

			switch (highlightType)
			{
			case HighlightType.Random:
				color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
				break;
			case HighlightType.Range:
				int red = 0;
				int green = 0;
				int blue = 0;

				red		= Random.Range(redMin, redMax + 1);
				green	= Random.Range(greenMin, greenMax + 1);
				blue	= Random.Range(blueMin, blueMax + 1);

				color = new Color((float)red / 255f, (float)green / 255f, (float)blue / 255f);
				break;
			case HighlightType.Custom:
				if (customColors.Count > 0)
				{
					color = customColors[Random.Range(0, customColors.Count)];
				}
				else
				{
					Debug.LogError("[CharacterGrid] Custom Colors is empty.");
				}
				break;
			}

			color.a			= highlightAlpha;
			highlight.color	= color;
		}

		private Text CreateFloatingText(string text, Color color, Vector2 position)
		{
			GameObject		floatingTextObject	= new GameObject("found_word_floating_text");
			RectTransform	floatingTextRectT	= floatingTextObject.AddComponent<RectTransform>();
			Text			floatingText		= floatingTextObject.AddComponent<Text>();

			floatingText.text		= text;
			floatingText.font 		= letterFont;
			floatingText.fontSize	= letterFontSize;
			floatingText.color		= color;

			floatingTextRectT.anchoredPosition	= position;
			floatingTextRectT.localScale		= new Vector3(currentScale, currentScale, 1f);
			floatingTextRectT.anchorMin 		= new Vector2(0f, 1f);
			floatingTextRectT.anchorMax 		= new Vector2(0f, 1f);
			floatingTextRectT.SetParent(gridOverlayContainer, false);

			ContentSizeFitter csf	= floatingTextObject.AddComponent<ContentSizeFitter>();
			csf.verticalFit			= ContentSizeFitter.FitMode.PreferredSize;
			csf.horizontalFit		= ContentSizeFitter.FitMode.PreferredSize;

			return floatingText;

		}

		private int AssignKeepSign(int a, int b)
		{
			int sign = (a < 0) ? -1 : 1;
			a = Mathf.Abs(b);
			a *= sign;
			return a;
		}

		#endregion
	}
}
