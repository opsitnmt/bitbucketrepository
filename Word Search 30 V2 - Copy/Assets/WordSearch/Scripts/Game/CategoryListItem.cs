﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WordSearch
{
	public class CategoryListItem : MonoBehaviour
	{
		#region Inspector Variables

		[SerializeField] private RectTransform	itemUI;
		[SerializeField] private CanvasGroup	itemCanvasGroup;
		[SerializeField] private Text			categoryNameText;
		[SerializeField] private Sprite			BackgroundImage;
        [SerializeField] private Button         SelectButton;

		#endregion

		#region Member Variables

		private const float animDuration	= 400f;
		private const float animStartY		= -150f;
		private const float animStartScale	= 0.8f;

		private int					index;
		private System.Action<int>	onButtonClicked;

        #endregion

        #region Public Methhardods

        public void Setup(string categoryName, Sprite bgImage, int index, WordSearchController.CategoryInfo categoryInfo,  int dificulityIndex, System.Action<int> onButtonClicked)
		{
			this.index				= index;
			this.onButtonClicked	= onButtonClicked;
            this.categoryInfos = categoryInfo;
            this.difIndex = dificulityIndex;
            this.BackgroundImage = bgImage;
            SelectButton.onClick.AddListener(SetDifficulity);
            SelectButton.onClick.AddListener(SetCategoryInfo);
            categoryNameText.text		= categoryName;
			//categoryIconImage.sprite	= categoryIcon;
		}

		public void OnClicked()
		{
			if (onButtonClicked != null)
			{
				onButtonClicked(index);
			}
		}
        //Since We don't have difficulity button anymore we have to set the difficulity from the category that is selected
        int difIndex;
        public void SetDifficulity()
        {
            WordSearchController.Instance.SetSelectedDifficulty(difIndex);
            WordSearchController.Instance.SetSelectedMode(0);
            
        }

        UIGameScreen uiGameScreen;

        WordSearchController.CategoryInfo categoryInfos;
        //Since we have more than one category based on difficulity we have to set the category that is selected
        //If you add more diffuculities for example "Extreme" don't forget to add to the dictinary in WordSearchController
        public void SetCategoryInfo()
        {
            WordSearchController.Instance.StartCategory(categoryInfos);

            if(uiGameScreen == null)
                uiGameScreen = FindObjectOfType<UIGameScreen>();

            uiGameScreen.SetUpGameTitle(categoryNameText.text);
            uiGameScreen.OnNewGameButtonClicked();

            if (BackgroundImage != null)
                uiGameScreen.BackgroundImage.sprite = BackgroundImage;
            else uiGameScreen.BackgroundImage.sprite = new Sprite();
        }

		public void StartShowAnimation(float animDelay)
		{
			itemUI.anchoredPosition	= new Vector2(0f, animStartY);
			itemUI.localScale		= new Vector3(animStartScale, animStartScale, 1f);
			itemCanvasGroup.alpha	= 0f;

			if (animDelay == 0)
			{
				StartShowAnimation();
			}
			else
			{
				StartCoroutine(WaitThenStartShowAnimation(animDelay));
			}
		}

		#endregion

		#region Private Methods

		private IEnumerator WaitThenStartShowAnimation(float animDelay)
		{
			yield return new WaitForSeconds(animDelay);

			StartShowAnimation();
		}

		private void StartShowAnimation()
		{
			Tween.PositionY(itemUI, Tween.TweenStyle.EaseOut, animStartY, 0f, animDuration).SetUseRectTransform(true);
			Tween.ScaleX(itemUI, Tween.TweenStyle.EaseOut, animStartScale, 1f, animDuration);
			Tween.ScaleY(itemUI, Tween.TweenStyle.EaseOut, animStartScale, 1f, animDuration);
			Tween.CanvasGroupAlpha(itemCanvasGroup, Tween.TweenStyle.EaseOut, 0f, 1f, animDuration);
		}

		#endregion
	}
}
