﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace WordSearch
{
	public class Popup : MonoBehaviour
	{
		#region Inspector Variables

		[SerializeField] protected int			animationDuration = 300;
		[SerializeField] protected Transform	uiContainer;
		[SerializeField] protected CanvasGroup	canvasGroup;

		#endregion

		#region Properties

		public bool IsDisplayed { get; set; }

		#endregion

		#region Unity Methods

		private void Start()
		{
			// Forse the popup to be hidden when it first starts
			SetDisplay(false, false, true);
		}

		#endregion

		#region Public Methods

		public void Show(bool animate = true)
		{
			SetDisplay(true, animate, false);
		}

		public void Hide(bool animate = true)
		{
			SetDisplay(false, animate, false);
		}

		#endregion

		#region Protected Methods

		protected void SetDisplay(bool display, bool animate, bool force)
		{
			if (!force && IsDisplayed == display)
			{
				return;
			}

			IsDisplayed = display;

			// Setup the canvas group to block clicks to background elements if the popup is being displayed
			canvasGroup.interactable	= display;
			canvasGroup.blocksRaycasts	= display;

			float fromScale	= display ? 0 : 1;
			float toScale	= display ? 1 : 0;

			float fromAlpha	= display ? 0f : 1f;
			float toAlpha	= display ? 1f : 0f;

			if (animate)
			{
				// Set the starting values
				uiContainer.localScale	= new Vector3(fromScale, fromScale, 1);
				canvasGroup.alpha		= fromAlpha;

				// Animate the X and Y scale
				Tween.ScaleY(uiContainer, Tween.TweenStyle.EaseOut, fromScale, toScale, animationDuration);
				Tween.ScaleX(uiContainer, Tween.TweenStyle.EaseOut, fromScale, toScale, animationDuration);

				// Fade in the popup
				Tween.CanvasGroupAlpha(canvasGroup, Tween.TweenStyle.EaseOut, fromAlpha, toAlpha, animationDuration);
			}
			else
			{
				uiContainer.localScale	= new Vector3(toScale, toScale, 1);
				canvasGroup.alpha		= toAlpha;
			}
		}

		#endregion
	}
}
