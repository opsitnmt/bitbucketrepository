﻿using UnityEngine;
using System.Collections;
using System.Threading;

namespace WordSearch
{
	public class AsyncTask : MonoBehaviour
	{
		#region Classes

		private class TaskInfo
		{
			private bool	isDone		= false;
			private object	isDoneLock	= new object();

			public bool IsDone
			{
				get
				{
					bool rtn;

					lock (isDoneLock)
					{
						rtn = isDone;
					}

					return rtn;
				}

				set
				{
					lock (isDoneLock)
					{
						isDone = value;
					}
				}
			}

			public object OutData { get; set; }
		}

		#endregion

		#region Member Variables

		private TaskInfo				taskInfo;
		private System.Action<object>	onFinishedCallback;

		#endregion

		#region Delegates

		public delegate object Task(object inData);

		#endregion

		#region Unity Methods

		private void Update()
		{
			if (taskInfo != null && taskInfo.IsDone)
			{
				// Call the onFinishedCallback in the main Unity thread passing it the OutData from the thread
				onFinishedCallback(taskInfo.OutData);

				// Destroy this AsyncTask object since it is no longer needed
				Destroy(gameObject);
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Creates a new AsyncTask object and starts the thread that will call task with inData. When the thread is finished
		/// AsyncTask will call onFinishedCallback from the main Unity thread.
		/// </summary>
		public static void Start(object inData, Task task, System.Action<object> onFinishedCallback)
		{
			new GameObject("AsyncTask", typeof(AsyncTask)).GetComponent<AsyncTask>().Run(inData, task, onFinishedCallback);
		}

		/// <summary>
		/// Creates the Thread ans starts it
		/// </summary>
		public void Run(object data, Task task, System.Action<object> onFinishedCallback)
		{
			this.onFinishedCallback = onFinishedCallback;

			// Create a new TaskInfo object
			taskInfo = new TaskInfo();

			// Create a new Thread object
			Thread thread = new Thread(() =>
			{
				taskInfo.OutData	= task(data);
				taskInfo.IsDone		= true;
			});

			// Start the thread
			thread.Start();
		}

		#endregion
	}
}
