﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portrait : MonoBehaviour {

	public GameObject btn1;
	public GameObject btn2;
	public GameObject btn3;
	public GameObject btn4;
	public GameObject btn5;
	public GameObject btn6;
	public GameObject btn7;
	public GameObject btn8;
	public GameObject btn9;
	public GameObject btn10;

	public GameObject failsafe;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.LandscapeRight;
		}
			
		if (btn1.activeSelf == true && failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.Portrait;
		}

		if (btn2.activeSelf == true && failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.Portrait;
		}

		if (btn3.activeSelf == true && failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.Portrait;
		}

		if (btn4.activeSelf == true && failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.Portrait;
		}

		if (btn5.activeSelf == true && failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.Portrait;
		}

		if (btn6.activeSelf == true && failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.Portrait;
		}

		if (btn7.activeSelf == true && failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.Portrait;
		}

		if (btn8.activeSelf == true && failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.Portrait;
		}

		if (btn9.activeSelf == true && failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.Portrait;
		}

		if (btn10.activeSelf == true && failsafe.activeSelf == false) {
			Screen.orientation = ScreenOrientation.Portrait;
		}

	}
}
