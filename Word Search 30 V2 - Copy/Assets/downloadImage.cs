﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class downloadImage : MonoBehaviour {

	public Texture2D myByte;

	// Use this for initialization
	public void downloadFreeImage(){
		NativeGallery.SaveImageToGallery (myByte, "Bible Wordsearch Fun", "Adam and Eve.png");
	}
}
